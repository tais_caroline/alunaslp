unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Memo2: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var texto : String;
begin
 texto := Memo1.Text;
 Memo2.Text := texto.Length.ToString;
end;

procedure TForm1.Button2Click(Sender: TObject);
var num : String;
begin
 num := Memo1.Text;
 Memo2.Text := num.Trim;
 end;

 procedure TForm1.Button3Click(Sender: TObject);
begin
Memo2.Text := LowerCase (Memo1.Text);
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
Memo2.Text := UpperCase (Memo1.Text);
end;

procedure TForm1.Button5Click(Sender: TObject);
var conteudo : String;
begin
 conteudo := Memo1.Text;
   Memo2.Text := conteudo.Replace ('e','a');
end;

procedure TForm1.Button6Click(Sender: TObject);
var conteudo : String;
begin
conteudo := Memo1.Text;
Memo2.Text :=  conteudo.contains('s').ToString;
end;

end.
