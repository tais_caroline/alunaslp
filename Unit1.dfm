object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 409
  ClientWidth = 747
  Color = clInactiveBorder
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 24
    Top = 24
    Width = 257
    Height = 377
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Button1: TButton
    Left = 296
    Top = 24
    Width = 145
    Height = 49
    Caption = 'Button1'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 298
    Top = 86
    Width = 145
    Height = 51
    Caption = 'Button2'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 298
    Top = 150
    Width = 143
    Height = 51
    Caption = 'Button3'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 298
    Top = 211
    Width = 145
    Height = 51
    Caption = 'Button4'
    TabOrder = 4
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 298
    Top = 268
    Width = 145
    Height = 53
    Caption = 'Button5'
    TabOrder = 5
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 300
    Top = 335
    Width = 143
    Height = 53
    Caption = 'Button6'
    TabOrder = 6
    OnClick = Button6Click
  end
  object Memo2: TMemo
    Left = 480
    Top = 24
    Width = 241
    Height = 377
    Lines.Strings = (
      'Memo2')
    TabOrder = 7
  end
end
